using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using Dapper;

namespace XZQ.Message.Web.User
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private readonly IDbConnection _conn =
            new SqlConnection(ConfigurationManager.ConnectionStrings["DbConnectionString"].ConnectionString);

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            var username = txtUserName.Text.Trim();
            var password = txtPassword.Text.Trim();

            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
            {
                lblMsg.ForeColor = Color.Red;
                lblMsg.Text = "用户名或密码不能为空，请输入内容！";
                return;
            }

            var p = new DynamicParameters();
            p.Add("usr_name", username);
            p.Add("usr_password", password);
            p.Add("usr_id", dbType: DbType.Int32, direction: ParameterDirection.Output);

            _conn.Execute("p_user_login", p, commandType: CommandType.StoredProcedure);
            var usrId = p.Get<int>("usr_id");

            if (usrId < 0)
            {
                lblMsg.ForeColor = Color.Red;
                lblMsg.Text = "用户名或密码不正确，请重新输入！";
                return;
            }

            lblMsg.ForeColor = Color.Blue;
            lblMsg.Text = "登录成功！";

            Session["id"] = p.Get<int>("usr_id");
            Session["UserName"] = p.Get<string>("usr_name");
        }
    }
}