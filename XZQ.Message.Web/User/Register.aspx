<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="XZQ.Message.Web.User.Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:TextBox ID="txtUserName" runat="server" placeholder="用户名" />
            <asp:TextBox ID="txtPassword" runat="server" placeholder="密码" TextMode="Password" />
            <asp:TextBox ID="txtPasswordConfirm" runat="server" placeholder="确认密码" TextMode="Password" />
            <asp:Button ID="btnRegister" runat="server" Text="注册" OnClick="btnRegister_Click" />
            <asp:HyperLink ID="hlLogin" runat="server" Text="登录" NavigateUrl="~/User/Login.aspx" />
            <asp:Label ID="lblMsg" runat="server" />
        </div>
    </form>
</body>
</html>
