using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using Dapper;


namespace XZQ.Message.Web.User
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private readonly IDbConnection _conn =
            new SqlConnection(ConfigurationManager.ConnectionStrings["DbConnectionString"].ConnectionString);

        /// <summary>
        /// 用户注册
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>三部曲
        /// 提交内容是否为空
        /// 提交内容是否存在
        /// 接收提交</remarks>
        protected void btnRegister_Click(object sender, EventArgs e)
        {
            var username = txtUserName.Text.Trim();
            var password = txtPassword.Text.Trim();
            var passwordConfirm = txtPasswordConfirm.Text.Trim();

            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
            {
                lblMsg.ForeColor = Color.Red;
                lblMsg.Text = "用户名或密码不能为空，请输入内容！";
                return;
            }

            if (!string.Equals(password, passwordConfirm))
            {
                lblMsg.ForeColor = Color.Red;
                lblMsg.Text = "两次密码不一致，请重新输入！";
                txtPassword.Text = txtPasswordConfirm.Text = string.Empty;
                return;
            }


            var p = new DynamicParameters();
            p.Add("@usr_name", username);
            p.Add("@usr_password", password);
            p.Add("@usr_id", dbType: DbType.Int32, direction: ParameterDirection.Output);

            _conn.Execute("p_user_register", p, commandType: CommandType.StoredProcedure);
            var usrId = p.Get<int>("@usr_id");

            if (0 < usrId)
            {
                lblMsg.ForeColor = Color.Blue;
                lblMsg.Text = "注册成功！";
            }
            else
            {
                lblMsg.ForeColor = Color.Red;
                lblMsg.Text = "注册失败，账户已存在！";
            }
        }
    }
}