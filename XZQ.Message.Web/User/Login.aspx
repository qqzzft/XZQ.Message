<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="XZQ.Message.Web.User.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:TextBox ID="txtUserName" runat="server" placeholder="用户名"></asp:TextBox>
            <asp:TextBox ID="txtPassword" runat="server" placeholder="密码" TextMode="Password"></asp:TextBox>
            <asp:Button ID="btnLogin" runat="server" Text="登录" OnClick="btnLogin_Click" />
            <asp:HyperLink ID="hlRegister" runat="server" Text="注册" NavigateUrl="~/User/Register.aspx" />
            <asp:Label ID="lblMsg" runat="server" />
        </div>
    </form>
</body>
</html>
