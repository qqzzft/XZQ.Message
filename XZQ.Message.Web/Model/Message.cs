using System;

namespace XZQ.Message.Web.Model
{
    public class Message
    {
        public int Msg_Id { get; set; }
        public string Msg_Title { get; set; }
        public string Msg_Content { get; set; }
        public DateTime? Msg_Datetime { get; set; }
        public int User_Id { get; set; }
    }
}