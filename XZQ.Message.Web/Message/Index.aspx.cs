using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using Dapper;
using System.Drawing;

namespace XZQ.Message.Web.Message
{
    public partial class Index : Page
    {
        private readonly IDbConnection _conn =
            new SqlConnection(ConfigurationManager.ConnectionStrings["DbConnectionString"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["id"] == null)
                {
                    hlLogin.Visible = true;
                    hlRegister.Visible = true;
                    lblUserName.Visible = false;
                    lbtnLogout.Visible = false;
                }
                else
                {
                    hlLogin.Visible = false;
                    hlRegister.Visible = false;
                    lblUserName.Visible = true;
                    lbtnLogout.Visible = true;
                    lblUserName.Text = Session["UserName"].ToString();
                }
            }

            LoadReapter();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtMsg.Text.Trim()))
            {
                lblMsg.ForeColor = Color.Red;
                lblMsg.Text = "请输入内容！";
                txtMsg.Focus();
                return;
            }

            if (Session["UserName"] == null)
            {
                Response.Redirect("~/User/Login.aspx");
                return;
            }

            MessageInsert();
        }

        /// <summary>
        /// 插入数据库
        /// </summary>
        private void MessageInsert()
        {
            var p = new DynamicParameters();
            p.Add("msg_titlte", "");
            p.Add("msg_content", txtMsg.Text.Trim());
            p.Add("usr_id", Session["id"]);
            p.Add("msg_id", dbType: DbType.Int32, direction: ParameterDirection.Output);

            _conn.Execute("p_msg_Add", p, commandType: CommandType.StoredProcedure);
            var result = p.Get<int>("msg_id");

            if (result > 0)
            {
                lblMsg.ForeColor = Color.Blue;
                lblMsg.Text = $"{DateTime.Now}：成功！";
                txtMsg.Text = string.Empty;
                LoadReapter();
            }
            else
            {
                lblMsg.ForeColor = Color.Red;
                lblMsg.Text = $"{DateTime.Now}：失败！";
            }
        }

        /// <summary>
        /// 加载Repeater
        /// </summary>
        private void LoadReapter()
        {
            var result = _conn.Query<Model.Message>("p_msg_list", commandType: CommandType.StoredProcedure);
            rpView.DataSource = result;
            rpView.DataBind();
        }

        protected void lbtnLogout_Click(object sender, EventArgs e)
        {
            Session["id"] = null;
            Session["username"] = null;
            Response.Redirect("~/Message/Index.aspx");
        }
    }
}