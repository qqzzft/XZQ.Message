<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="XZQ.Message.Web.Message.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>留下您的足迹 - 留言板</title>
</head>
<body>
    <form id="form1" runat="server">
        <%--登录状态--%>
        <div>
            [<asp:HyperLink ID="lblUserName" runat="server" Text="登录" />
            <asp:HyperLink ID="hlRegister" runat="server" Text="注册" NavigateUrl="~/User/Register.aspx" />|
            <asp:LinkButton ID="lbtnLogout" runat="server" Text="注销" OnClick="lbtnLogout_Click" />
            <asp:HyperLink ID="hlLogin" runat="server" Text="登录" NavigateUrl="~/User/Login.aspx" />]
        </div>
        <%--输入--%>
        <div>
            <asp:TextBox ID="txtMsg" runat="server"></asp:TextBox>
            <asp:Button ID="btnSubmit" runat="server" Text="留言" OnClick="btnSubmit_Click" />
            <asp:Label ID="lblMsg" runat="server" />
        </div>
        <%--列表--%>
        <div>
            <asp:Repeater ID="rpView" runat="server">
                <ItemTemplate>
                    <div>
                        <div><%#Eval("Msg_Id") %></div>
                        <div><%#Eval("Msg_Datetime") %></div>
                        <div><%#Eval("msg_content") %></div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </form>
</body>
</html>
