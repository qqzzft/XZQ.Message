# <ruby>留言板<rt>Message Board</rt></ruby>

- 框架：.NET Framework 4.5.2
- 模板：ASP.NET WebForm
- 建模：PowerDesigner
- 数据库：mysql
- IDE：vs2017、rider
- nuget：Dapper、MySql

## 文件夹结构


## 说明

- Dapper要求`.NET Framework`最低版本为 4.5.1
- MySql要求`.NET Framework`最低版本为 4.5.2
- Microsoft.AspNet.Mvc.5.2.6.nupkg `.NET Framework`版本为 4.5
- 官方提供下载的`.NET Framework`[下载](https://www.microsoft.com/net/download/all)版本分别有（截止2018-06-20）
  - 4.7.2
  - 4.7.1
  - 4.7
  - 4.6.2
  - 4.6.1
  - 4.5.2
  - 3.5 SP1
- XP支持`.NET Framework`的最高版本为 4.0

## 其他
