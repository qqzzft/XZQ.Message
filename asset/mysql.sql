CREATE TABLE `user` (
	`user_id` INT(11) NOT NULL AUTO_INCREMENT,
	`user_name` VARCHAR(50) NOT NULL,
	`user_password` VARCHAR(100) NOT NULL,
	PRIMARY KEY (`user_id`),
	UNIQUE INDEX `user_name` (`user_name`)
)
COMMENT='用户信息表'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
CREATE TABLE `message` (
	`msg_id` INT(11) NOT NULL AUTO_INCREMENT,
	`msg_content` TEXT NOT NULL,
	`msg_datetime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`user_id` INT(11) NOT NULL,
	PRIMARY KEY (`msg_id`),
	INDEX `FK_message_user` (`user_id`),
	CONSTRAINT `FK_message_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
)
COMMENT='留言信息表'
ENGINE=InnoDB
;
