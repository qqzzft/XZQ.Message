use message_board
--use message_board_iis
go
/* 用户注册，成功返回id，失败返回-1
   参考：https://www.cnblogs.com/alexyu/articles/1719377.html
 */
create proc p_user_register
    @usr_name nvarchar(50),
    @usr_password varchar(100),
    @usr_id int output
as
begin
    --判断是否存在账号
    if exists(select * from t_user where usr_name=@usr_name)
        --存在，返回-1
        select @usr_id=-1
    else
    begin
        insert into t_user(usr_name,usr_password) values(@usr_name,@usr_password)
        select @usr_id=SCOPE_IDENTITY()
    end
end
go
/* 用户登录，成功返回id，失败返回-1
   参考：https://www.cnblogs.com/alexyu/articles/1719377.html
 */
create proc p_user_login
    @usr_name nvarchar(50),
    @usr_password varchar(100),
    @usr_id int output
as
begin
    --判断是否存在账号
    if exists(select * from t_user where usr_name=@usr_name)
        select @usr_id=usr_id from t_user
        where usr_name=@usr_name and usr_password=@usr_password
    else
        --不存在
        select @usr_id=-1
end
go
/* 修改密码，成功返回id，失败返回-1
   参考：https://www.cnblogs.com/alexyu/articles/1719377.html
 */
create proc p_user_update_pwd
    @usr_id int,
    @usr_password varchar(100),
    @usr_password_new varchar(100),
    @result int output
as
begin
    update t_user set usr_password=@usr_password_new
    where usr_id=@usr_id and usr_password=@usr_password
    if @@rowcount>=1
        select @result=1
    else
        select @result=-1
end
go
/* 初始数据
 */
declare @result int
exec p_user_register 'admin','admin',@result output
exec p_user_register 'boy','boy',@result output
print @result