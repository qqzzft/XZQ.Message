use message_board
--use message_board_iis
go
--用户信息表
create table t_user(
    usr_id int identity(1,1)
    ,usr_name nvarchar(50) not null
    ,usr_password varchar(100) not null
    ,primary key(usr_id)
    ,unique(usr_name)
)
go
--留言信息表
create table t_message(
    msg_id int identity(1,1)
    ,msg_titlte nvarchar(50)
    ,msg_content ntext
    ,msg_datetime datetime not null default getdate()
    ,usr_id int not null
    ,foreign key(usr_id) references t_user(usr_id)
)
go