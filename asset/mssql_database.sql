/* 新建数据库
 */
use master
go
create database message_board on(
    name=N'message_board',
    filename=N'D:\userFolder\localdb\message_board.mdf'
    --filename=N'D:\userFolder\localdb\iisdb\message_board.mdf'
)
go
/* 附加已有数据库
 */
use master
go
create database message_board
on(
    filename=N'D:\userFolder\localdb\message_board.mdf'
    --filename=N'D:\userFolder\localdb\iisdb\message_board.mdf'
)
for attach
go
/* 新建用户
 */
use master
go
create login ball with PASSWORD=N'222222'--, DEFAULT_DATABASE=message_board, CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
go
use message_board
go
create user ball for login ball
go
use message_board
go
alter role db_owner add MEMBER ball
go
/* 启用 sa，密码为222222
 */
use master
go
ALTER LOGIN sa WITH DEFAULT_DATABASE=master, CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
go
use master
go
ALTER LOGIN sa WITH PASSWORD=N'222222'
go
ALTER LOGIN sa ENABLE
go
