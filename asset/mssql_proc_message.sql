use message_board
--use message_board_iis
go
/* 发表留言，返回id
 */
create proc p_msg_Add
    @msg_titlte nvarchar(50),
    @msg_content ntext,
    @usr_id int,
    @msg_id int output
as
begin
    insert into t_message(msg_titlte,msg_content,usr_id) values(@msg_titlte,@msg_content,@usr_id)
    select @msg_id=scope_identity()
end
go
/* 留言列表
 */
create proc p_msg_list
as
begin
    select * from t_message order by msg_id desc
end
go